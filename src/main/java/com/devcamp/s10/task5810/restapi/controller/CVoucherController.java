package com.devcamp.s10.task5810.restapi.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s10.task5810.restapi.model.CVoucher;
import com.devcamp.s10.task5810.restapi.repository.IVoucherRepository;

@RestController
public class CVoucherController {
    @Autowired
    IVoucherRepository iVoucherRepository;

    @CrossOrigin
    @GetMapping("/vouchers")
    public ResponseEntity<List<CVoucher>> getAllVouchers(){
        try {
            List<CVoucher> listVoucher = new ArrayList<CVoucher>();
            iVoucherRepository.findAll().forEach(listVoucher :: add);
            return new ResponseEntity<List<CVoucher>>(listVoucher, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
