package com.devcamp.s10.task5810.restapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.s10.task5810.restapi.model.CVoucher;

public interface IVoucherRepository extends JpaRepository<CVoucher, Long> {
    
}
